**The Admissions Guide (TAG)**


---

## Elevator pitch
"Applying to colleges should be fun. It has become a rite of passage, but one that is filled with stress and anxiety. The Admissions Guide is here to change that. We want this to be the time of your life. We help you realize the gifts you bring and discover schools that will help you thrive. There are 5,000 schools where you can continue to grow and become who you are meant to be. Our approach is simple.  We start by getting to know you and helping you tell the story of what makes you unique, what makes you tick. You determine the time and for how long we work together. Let The Admissions Guide be your compass and come take the journey with us.   Applying to colleges has never been this much fun."

---

## Repo

1. 
2. 
3. 
4. 
5. 

---
